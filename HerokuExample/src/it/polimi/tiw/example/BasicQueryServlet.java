package it.polimi.tiw.example;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/BasicQueryServlet")
public class BasicQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection = null;

	public void init() {
		try {
			//You might need to move this to the doGet/doPost of your servlets
			connection = ConnectionHandler.getConnection(getServletContext());
		} catch (UnavailableException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String query = "SELECT firstname, lastname, city FROM person";
		ResultSet result = null;
		Statement statement = null;
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();

		try {
			if (connection == null){
				throw new SQLException("No connection availble");
			}
			statement = connection.createStatement();
			result = statement.executeQuery(query);
			while (result.next()) {
				out.println("Firstname: " + result.getString("firstname") + " Lastname: " + result.getString("lastname")
						+ " City: " + result.getString("city"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			out.append("SQL ERROR");
		} finally {
			try {
				result.close();
			} catch (Exception e1) {
				e1.printStackTrace();
				out.append("SQL RES ERROR");
				out.append(e1.toString());
			}
			try {
				statement.close();
			} catch (Exception e1) {
				out.append("SQL STMT ERROR");
			}

		}
	}

	public void destroy() {
		ConnectionHandler.closeConnection(connection);
	}
}
