create table person
(
    id        serial  not null
        constraint person_pk
            primary key,
    lastname  varchar not null,
    firstname varchar not null,
    city      varchar not null
);

/*alter table person
    owner to phfudhpkfnciep;*/

create unique index person_id_uindex
    on person (id);

INSERT INTO public.person (id, lastname, firstname, city) VALUES (1, 'fraternali', 'piero', 'Como');
INSERT INTO public.person (id, lastname, firstname, city) VALUES (2, 'milani', 'federico', 'Como');
INSERT INTO public.person (id, lastname, firstname, city) VALUES (3, 'torres', 'rocio', 'La Plata');