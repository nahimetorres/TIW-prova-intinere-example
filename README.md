
# Mission Expenses Management

Description of the project and functionalities

## Material

* NOME_STUDENTE_proggetazione: it contains the design choices and architecture of the project
* init.sql: an sql script to inizilize the DB with the structure and test data
* HerokuExample: the Eclipse project

* [URL of the deployed project](https://test-prova-intinere.herokuapp.com/ConnectionTester)


### Prerequisites

You need to have a Postgress database, Eclipse and Tomcat configured.

## Getting Started

To have this project up and running in your machine you need to complete the following steps:

1.  Import the database structure and test data from init.sql 
2.  Import project into Eclipse (File -> Import -> Existing project into workspace -> Choose project in your PC)
3.  Change username and password to work with your configuration (WebContent/WEB-INF/web.xml, change dbUser and dbPassword)
4.  Run the project (Run -> Run as... -> Run on Server, choose server configuration and Finish)

## Additional Information

...

## Authors

* **Piero Fraternali - Federico Milani - Rocio Nahime Torres** 
